export const state = () => ({
    user: "",
    artist: ""
})

export const mutations = {
    setUser(state, user) {
        state.user = user
    },
    setArtist(state, artist) {
        state.artist = artist
    },
}

export const actions = {
    setUser({ commit }, artist) {
        commit('setUser', artist)
    },
    setArtist({ commit }, artist) {
        commit('setArtist', artist)
    },
}


export const getters = {
    user: state => {
        return state.user
    },
    artist: state => {
        return state.artist
    },
}