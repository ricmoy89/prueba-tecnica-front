export const saveRanking = async (instance, star, cancion) => {

    const ranking = (cancion.ranking && star != cancion.ranking.split("/")[0]) || !cancion.ranking ? `${star}/5` : 0

    await instance.$axios
        .post("favoritos", {
            usuario: instance.$store.getters.user,
            ranking: ranking,
            cancion_id: cancion.cancion_id,
            nombre_banda: cancion.artista,
        })
        .then((res) => {
            cancion.ranking = ranking;
        });

}

export const getIconRanking = (index, ranking) => {
    if (ranking && ranking.split("/")[0] >= index) {
        return "star-fill";
    }
    return "star";
}