export default function ({ $axios, redirect, store, error, app }) {
    $axios.onRequest((config) => {
        config.baseURL = process.env.API_ENDPOINT
    })
}