# Información de bandas musicales
```
Te pediremos construir:
    * Una aplicación backend que permita consultar canciones de una banda músical y que permitas guardar una canción como favoritos
    * Una aplicación web que consuma el primer endpoint de la aplicación backend
```
### Pre-requisitos 📋

_Aplicaciones necesitas para que el software funcione_

```
NodeJs https://nodejs.org/es/
```

### Instalación 🔧

_Para realizar la instalacion se deben realizar los siguientes pasos_

_Clonar el repositorio_

```
HTTPS: lone https://ricmoy89@bitbucket.org/ricmoy89/prueba-tecnica-front.git
SSH: git clone git@bitbucket.org:ricmoy89/prueba-tecnica-front.git

```
_Instalar las dependencias_

```
npm install


```
_Iniciar la aplicacion_

```
npm start


```
_Compilar la aplicacion_

```
npm run build
```

_Finalmente ingresar a la url http://localhost:3000 en caso de haber realizado una instalacion local con los valores predeterminados_

## Construido con 🛠️

* [VueJS](https://vuejs.org/)


## Autor ✒️


* **Ricardo Moyla**